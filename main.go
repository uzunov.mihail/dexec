package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/urfave/cli/v2"
	"go.uber.org/zap"
	"golang.org/x/crypto/ssh"
)

var (
	logsFolder = filepath.Join(".", "logs")
)

type Config struct {
	Device       string   `json:"Device"`
	SshHost      string   `json:"SshHost"`
	SshPort      string   `json:"SshPort"`
	SshUser      string   `json:"SshUser"`
	SshPassword  string   `json:"SshPassword"`
	Commands     []string `json:"Commands"`
	PostCommands []string `json:"PostCommands"`
}

var globalLogger *zap.Logger

func main() {
	globalLogger, _ = zap.NewProduction()
	defer globalLogger.Sync()

	if err := os.RemoveAll(logsFolder); err != nil {
		globalLogger.Error("failed to delete previous logs", zap.Error(err))
	}

	app := initApp()

	if err := app.Run(os.Args); err != nil {
		globalLogger.Fatal("failed to run the app", zap.Error(err))
	}
}

// NewLogger creates a new logger which creates a new log file and writes to it. The name of the file is the provided device name.
func NewLogger(device string) (*zap.Logger, error) {
	path := filepath.Join(logsFolder, device+".log")
	err := os.MkdirAll(logsFolder, os.ModePerm)
	if err != nil {
		panic("cannot create folder logs: " + err.Error())
	}

	f, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	f.Close()

	cfg := zap.NewProductionConfig()
	cfg.OutputPaths = []string{
		path,
	}
	return cfg.Build()
}

// dexec is the main handler of the application.
func dexec(c *cli.Context) error {
	// 1. Read the config file
	jsonData, err := getJsonContent(c.String("file"))
	if err != nil {
		return err
	}

	configs := []Config{}

	json.Unmarshal(jsonData, &configs)

	var wg sync.WaitGroup

	// 2. Iterate the devices and run sshCommand with provided args
	for _, config := range configs {
		wg.Add(1)
		go executeCommand(&wg, config)
	}

	wg.Wait()

	return nil
}

// initApp initializes a new cli application.
func initApp() *cli.App {
	return &cli.App{
		Name:  "dexec",
		Usage: "takes an input file configuration of devices and commands and runs them",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "file",
				Value: filepath.Join(".", "examples", "two.json"),
				Usage: "sets the path to the config",
			},
		},
		Action: dexec,
	}
}

// getJsonContent returns the data of the json file in string format.
func getJsonContent(path string) ([]byte, error) {
	jsonFile, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer jsonFile.Close()

	data, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func executeCommand(wg *sync.WaitGroup, config Config) {
	globalLogger.Info("Executing device", zap.String("device", config.Device))
	logger, err := NewLogger(config.Device)
	if err != nil {
		log.Fatalf("failed to create logger: %s\n", err.Error())
	}

	sshConfig := &ssh.ClientConfig{
		User: config.SshUser,
		Auth: []ssh.AuthMethod{
			ssh.Password(config.SshPassword),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(), // This is inscure, use only for testing.
	}

	conn, err := ssh.Dial("tcp", fmt.Sprintf("%s:%s", config.SshHost, config.SshPort), sshConfig)
	if err != nil {
		logger.Fatal("failed to dial", zap.String("device", config.Device), zap.Error(err))
	}
	defer conn.Close()

	session, err := conn.NewSession()
	if err != nil {
		logger.Fatal("failed to create new session", zap.String("device", config.Device), zap.Error(err))
	}
	defer session.Close()

	outfile, err := os.OpenFile(fmt.Sprintf("./logs/%s.log", config.Device), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		panic(err)
	}
	defer outfile.Close()

	stdout, err := session.StdoutPipe()
	if err != nil {
		globalLogger.Fatal("failed to get out pipe", zap.String("device", config.Device), zap.Error(err))
	}

	stdin, err := session.StdinPipe()
	if err != nil {
		globalLogger.Fatal("failed to get in pipe", zap.String("device", config.Device), zap.Error(err))
	}

	err = session.Shell()
	if err != nil {
		logger.Fatal("failed to get shell", zap.String("device", config.Device), zap.Error(err))
	}

	stdinLines := make(chan string)
	go func() {
		scanner := bufio.NewScanner(stdout)
		for scanner.Scan() {
			stdinLines <- scanner.Text()
		}
		if err := scanner.Err(); err != nil {
			globalLogger.Fatal("scanner failed", zap.Error(err))
		}
		close(stdinLines)
	}()

	runCommandRange(stdin, stdout, logger, outfile, config.Device, config.Commands, stdinLines)
	runCommandRange(stdin, stdout, logger, outfile, config.Device, config.PostCommands, stdinLines)

	stdin.Close()

	err = session.Wait()
	if err != nil {
		log.Fatal(err)
	}

	wg.Done()
}

func runCommandRange(stdin io.WriteCloser, stdout io.Reader, logger *zap.Logger, outfile *os.File, device string, cmds []string, stdinLines chan string) {
	var strTmp string

	for _, cmd := range cmds {
		logger.Info("running command", zap.String("cmd", cmd))
		_, err := stdin.Write([]byte(cmd + "\n"))
		if err != nil {
			log.Fatal(err)
		}

		timer := time.NewTimer(0)
	InputLoop:
		for {
			timer.Reset(time.Second)
			select {
			case line, ok := <-stdinLines:
				if !ok {
					globalLogger.Info("finished processing commands")
					break InputLoop
				}
				strTmp += line
				strTmp += "\n"
			case <-timer.C:
				break InputLoop
			}
		}
		outfile.WriteString(strTmp)
		strTmp = ""
	}
}
