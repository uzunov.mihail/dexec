# dexec
An application that take input of devices and runs shell commands against them.

## Requirements
* Golang ^1.17
* Docker

## Example configuration file
Inside the examples folder you can find sample configuration to run the application.

Specifically for that config to work you will need Docker and starting two instances of ssh server
containers. 

You can do that by running these:
1. `docker run  --publish=2222:22 sickp/alpine-sshd:7.5-r2`
2. `docker run  --publish=2223:22 sickp/alpine-sshd:7.5-r2`

This will run two containers listening respectively on ports 2222 and 2223.

After that it should be possible to run the application with default settings.

`./dexec`

## How to build
### Linux and macOS
`go build -o dexec main.go`

### Windows
`go build -o dexec.exe main.go`

## Running the built application
You can run the application by simply `./dexec` with default settings.

If you want to provide a different file you can do that by
`./dexec --file ${PATH}`

or for example
`./dexec --file ./my-configs/subfolder/config.json`

